import {
    DELETE_ACTIVITY_BEGIN,
    DELETE_ACTIVITY_SUCCESS,
    DELETE_ACTIVITY_ERROR
  } from "../actions";
  const initialState = {
    getDeleteActivity: false,
    usersDeleteActivity: []
  };
  
const deleteactivityreducer = (state = initialState, action) => {
    switch (action.type) {
      case DELETE_ACTIVITY_BEGIN:
        return {
          ...state,
          getDeleteActivity: true
        };
      case DELETE_ACTIVITY_SUCCESS:
        return {
          ...state,
          getDeleteActivity: false,
          usersDeleteActivity: action.data
        };
      case DELETE_ACTIVITY_ERROR:
        return {
          ...state,
          getDeleteActivity: false
        };
      default:
        return state;
    }
  };
  
  export default deleteactivityreducer;
  