import {
    GET_ACTIVITY_BEGIN,
    GET_ACTIVITY_SUCCESS,
    GET_ACTIVITY_ERROR
  } from "../actions";
  
  const initialState = {
    getUsersActivity: false,
    usersActivity: []
  };
  
const ma = (state = initialState, action) => {
    switch (action.type) {
      case GET_ACTIVITY_BEGIN:
        return {
          ...state,
          getUsersActivity: true
        };
      case GET_ACTIVITY_SUCCESS:
        return {
          ...state,
          getUsersActivity: false,
          usersActivity: action.data
        };
      case GET_ACTIVITY_ERROR:
        return {
          ...state,
          getUsersActivity: false
        };
      default:
        return state;
    }
  };
  
  export default ma;