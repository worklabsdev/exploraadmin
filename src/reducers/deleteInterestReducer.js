import {
  DELETE_INTEREST_BEGIN,
  DELETE_INTEREST_SUCCESS,
  DELETE_INTEREST_ERROR
} from '../actions';
const initialState = {
  getDeleteInterest: false,
  usersDeleteInterest: []
};

const deleteinterestreducer = (state = initialState, action) => {
  switch (action.type) {
    case DELETE_INTEREST_BEGIN:
      return {
        ...state,
        getUsersActivity: true
      };
    case DELETE_INTEREST_SUCCESS:
      return {
        ...state,
        getUsersActivity: false
      };
    case DELETE_INTEREST_ERROR:
      return {
        ...state,
        getUsersActivity: false
      };
    default:
      return state;
  }
};

export default deleteinterestreducer;
