import {
  LOGIN_SUCCESS,
  LOGIN_BEGIN,
  LOGIN_ERROR,
  INVALID_LOGIN,
  RESET_STATE
} from "../actions";

const initialState = {
  isLoggedin: false,
  loading: false,
  loginError: ""
};
const login = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_BEGIN:
      return {
        ...state,
        loading: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedin: true,
        loading: false,
        loginData: action.data
      };
    case INVALID_LOGIN:
      return {
        ...state,
        loading: false,
        isLoggedin: false,
        loginData: action.data
      };
    case LOGIN_ERROR:
      return {
        ...state,
        isLoggedin: false,
        loading: false,
        loginError: action.error
      };
    case RESET_STATE:
      return initialState;
    default:
      return state;
  }
};

export default login;
