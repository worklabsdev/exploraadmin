import { combineReducers } from "redux";
import login from "./loginReducer";
import interest from "./interestReducer";
import users from "./userReducer";
import ma from "./manageactivityReducer";
import deleteactivityreducer from "./deleteInterestReducer";
import deleteinterestreducer from "./deleteActivityReducer";
export default combineReducers({ login, interest, users, ma, deleteactivityreducer, deleteinterestreducer });