import {
  SAVE_INTEREST_BEGIN,
  SAVE_INTEREST_SUCCESS,
  SAVE_INTEREST_ERROR,
  GET_INTEREST_BEGIN,
  GET_INTEREST_SUCCESS,
  GET_INTEREST_ERROR
} from "../actions";

const initialState = {
  interestSaved: false,
  interestLoading: false,
  getInterestDataLoading: false,
  interestData: []
};

const interest = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_INTEREST_BEGIN:
      return {
        ...state,
        interestLoading: true,
        interestSaved: false
      };
    case SAVE_INTEREST_SUCCESS:
      return {
        ...state,
        interestLoading: false,
        interestSaved: true
        // data: action.data
      };
    case SAVE_INTEREST_ERROR:
      return {
        ...state,
        interestLoading: false,
        interestSaved: false
      };
    case GET_INTEREST_BEGIN:
      return {
        ...state,
        getInterestDataLoading: true
      };
    case GET_INTEREST_SUCCESS:
      return {
        ...state,
        getInterestDataLoading: false,
        interestData: action.data
      };
    case GET_INTEREST_ERROR:
      return {
        ...state,
        getInterestDataLoading: false
      };
    default:
      return state;
  }
};

export default interest;
