import React, { Component } from "react";
import Header from "../Header";
import Sidebar from "../Sidebar";
import {
  Button,
  Form,
  FormGroup,
  Label,
  Input,
  Row,
  Col,
  Container
} from "reactstrap";

class AddInterest extends Component {
  render() {
    return (
      <div>
        <Header toggle={this.toggleDropdown} />
        <Row>
          <Col xs="4" sm="2" className="sidebarstyle">
            <Sidebar />
          </Col>
          <Col sm="12" md={{ size: 4, offset: 2 }}>
            <Container>
              <Form>
                <FormGroup>
                  <Label for="Interest">Interest</Label>
                  <Input
                    type="text"
                    name="interest"
                    id="interest"
                    placeholder="Enter Interest"
                  />
                </FormGroup>

                <Button style={{marginTop:"3%"}}> Submit</Button>
              </Form>
            </Container>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = {};

export default AddInterest;
