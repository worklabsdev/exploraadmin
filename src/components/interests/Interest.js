import React, { Component } from 'react';
import Header from '../Header';
import Sidebar from '../Sidebar';
import { saveInterest, getInterest } from '../../actions';
import { connect } from 'react-redux';
import { getDeleteInterest } from '../../actions/index';
import {
  Row,
  Col,
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Input
} from 'reactstrap';

class Interest extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openNavDropDown: false,
      modalIModal: false,
      modalIModalDel: false,
      modalIModalEdit: false,
      interestname: '',
      idToDelete: ''
    };
  }

  componentDidMount() {
    this.props.dispatch(getInterest());
    // this.forceUpdate();
  }
  toggleDropdown = () => {
    this.setState({
      openNavDropDown: !this.state.openNavDropDown
    });
  };
  toggleAddInterestModal = () => {
    this.setState(prevState => ({
      modalIModal: !prevState.modalIModal
    }));
  };
  toggleAddInterestModalDel = id => {
    this.setState(prevState => ({
      modalIModalDel: !prevState.modalIModalDel,
      idToDelete: id
    }));
  };
  toggleAddInterestModalEdit = name => {
    this.setState(prevState => ({
      modalIModalEdit: !prevState.modalIModalEdit,
      interestName: name
    }));
  };
  handleChange = e => {
    console.log(e);
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = e => {
    var interestname = this.state.interestname;
    var interestjson = {
      interestname: interestname
    };
    var { dispatch } = this.props;
    dispatch(saveInterest(interestjson));
  };
  // componentWillReceiveProps
  // componentWillReceiveProps() {
  //   if (this.props.interestSaved && this.props.interestSaved == true) {
  //     this.toggleAddInterestModal();
  //   }
  // }
  componentDidUpdate() {
    if (this.props.interestLoading == true) {
      this.props.dispatch(getInterest());
      // this.forceUpdate();
    }
  }
  deleteinterest = () => {
    const { idToDelete } = this.state;
    this.props.dispatch(getDeleteInterest(idToDelete));

    // window.location.reload();
  };

  render() {
    console.log('props', this.props);
    return (
      <div>
        <Header toggle={this.toggleDropdown} />
        <Row>
          <Col xs="4" sm="2" className="sidebarstyle">
            <Sidebar />
          </Col>
          <Col>
            <Container>
              <h2 className="down" align="left">
                Interest's
                <Button
                  onClick={this.toggleAddInterestModal}
                  className="addinterestbtn"
                >
                  + Add Interest
                </Button>
              </h2>
              <Table>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>Interest Name</th>
                    {/* <th>Created At</th> */}
                    <th>Action's</th>
                  </tr>
                </thead>

                {this.props.getInterestDataLoading == true ? (
                  <div>Loading...</div>
                ) : (
                  <tbody>
                    {this.props.interestData &&
                      this.props.interestData.length > 0 &&
                      this.props.interestData.map((item, index) => {
                        console.log('-==========' + item);
                        return (
                          <tr key={item._id}>
                            <td>{index + 1}</td>
                            <td>{item.interestName}</td>
                            {/* <td>{item.createdAt}</td> */}
                            <td>
                              <i
                                className="fa fa-edit"
                                onClick={() =>
                                  this.toggleAddInterestModalEdit(
                                    item.interestName
                                  )
                                }
                              />{' '}
                              &nbsp;{' '}
                              <i
                                className="fa fa-trash"
                                onClick={() =>
                                  this.toggleAddInterestModalDel(item._id)
                                }
                              />
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                )}
              </Table>
            </Container>
          </Col>
        </Row>
        <Modal
          isOpen={this.state.modalIModal}
          toggle={this.toggleAddInterestModal}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleAddInterestModal}>
            Modal title
          </ModalHeader>
          <ModalBody>
            <Label for="Interest">Interest</Label>
            <Input
              type="text"
              name="interestname"
              value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              placeholder="Enter Interest"
            />
            <Button className="pull-right" onClick={e => this.handleSubmit(e)}>
              Submit{' '}
              {this.props.interestLoading &&
              this.props.interestLoading == true ? (
                <i className="fa fa-spinner fa-spin" />
              ) : (
                ''
              )}
            </Button>
          </ModalBody>
        </Modal>

        <Modal
          isOpen={this.state.modalIModalEdit}
          toggle={this.toggleAddInterestModalEdit}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleAddInterestModalEdit}>
            Edit Interest Name
          </ModalHeader>
          <ModalBody>
            {/* <Label for="Interest">Interest</Label> */}
            <Input
              type="text"
              name="interestname"
              value={this.state.interestName}
              onChange={e => this.handleChange(e)}
            />
            <Button
              className="pull-right"
              style={{ marginTop: '4%' }}
              onClick={e => this.handleSubmit(e)}
            >
              Ok
              {this.props.interestLoading &&
              this.props.interestLoading == true ? (
                <i className="fa fa-spinner fa-spin" />
              ) : (
                ''
              )}
            </Button>
            {/* </Form> */}
          </ModalBody>
        </Modal>

        <Modal
          isOpen={this.state.modalIModalDel}
          toggle={this.toggleAddInterestModalDel}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleAddInterestModalDel}>
            Delete
          </ModalHeader>
          <ModalBody>
            <Label for="Interest">Are You sure to Delete !!</Label>
            <Button
              className="btn btn-danger"
              style={{ marginLeft: '85%' }}
              onClick={this.deleteinterest}
            >
              Delete
            </Button>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    interestSaved: state.interest.interestSaved,
    interestLoading: state.interest.interestLoading,
    interestData: state.interest.interestData,
    getInterestDataLoading: state.interest.getInterestDataLoading,

    getDeleteInterest: state.deleteinterestreducer.getDeleteInterest,
    usersDeleteInterest: state.deleteinterestreducer.usersDeleteInterest
  };
};

export default connect(mapStateToProps)(Interest);
