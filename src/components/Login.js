import React, { Component } from "react";
import { connect } from "react-redux";
import {
  Container,
  Row,
  Col,
  Button,
  Alert,
  Form,
  FormGroup,
  Label,
  Input,
} from "reactstrap";
import { login_admin } from "../actions";

import explora from "../images/explora.png";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      bgColorRecent: "red",
      bgColor: "white",
    };
  }
  handleChange(e) {
    console.log(e);
    this.setState({ [e.target.name]: e.target.value });
  }
  login = event => {
    event.preventDefault();
    var email = this.state.username;
    var password = this.state.password;
    var userjson = {
      email,
      password,
    };
    var { dispatch } = this.props;
    dispatch(login_admin(userjson));
  };

  componentDidUpdate() {
    // console.log(this.props.isLoggedIn);
    console.log("This is data------", this.props);
    if (
      this.props.isLoggedIn &&
      this.props.isLoggedIn == true &&
      this.props.status == 200
    ) {
      this.props.history.push("/interests");
    } else {
      // console.log("This is data------", this.props.status);
      // console.log("checking componentwillreceive props");
    }
  }
  componentDidMount() {
    this.setState({ bgColorRecent: "#ff0000" });
  }
  render() {
    const recentStyles = {
      backgroundColor: this.state.bgColorRecent || "",
    };
    return (
      <Container className="shiftinglogin">
        <Form onSubmit={this.login}>
          {(this.props.status && this.props.status == 401) ||
          this.props.status == 404 ? (
            <Alert
              color="danger"
              style={{
                position: "absolute",
                marginTop: "-3.6%",
                marginLeft: "34%",
              }}
            >
              Invalid Credentials
            </Alert>
          ) : (
            ""
          )}
          <Row className="loginform">
            <Col
              sm="12"
              md={{ size: 4, offset: 4 }}
              style={recentStyles}
              id="col-recent"
              className="recent clickable"
              onClick={this.componentDidMount.bind(this)}
            >
              <img src={explora}
                style={{ width: 130, height: 45,marginTop:"8%"}}
                className="imagelogin"
              />

              <FormGroup>
                <Label />
                <Input
                  placeholder="username"
                  style={{
                    width: 300,
                    height: 30,
                    marginLeft: "7%",
                  }}
                  name="username"
                  value={this.state.username}
                  onChange={e => this.handleChange(e)}
                />
              </FormGroup>
              <FormGroup>
                <Label />
                <Input
                  style={{
                    width: 300,
                    height: 30,
                    marginLeft: "7%",
                  }}
                  placeholder="password"
                  name="password"
                  value={this.state.password}
                  type="password"
                  onChange={e => this.handleChange(e)}
                />
              </FormGroup>
              <Label />
              <Button
                type="submit"
                color="success"
                style={{
                  marginBottom: "3%",
                  backgroundColor: this.state.bgColor,
                  color: "black",
                  borderColor: "white",
                  width: 70,
                  height: 36,
                }}
              >
                Login
                {this.props.loading == true ? (
                  <i className="fa fa-spinner fa-spin" style={{marginLeft:"150%", marginBottom:"50%"}} />
                ) : (
                  ""
                )}
              </Button>
            </Col>
          </Row>
        </Form>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.login.isLoggedin,
    loading: state.login.loading,
    loginData: state.login.loginData,
    status: state.login.loginData ? state.login.loginData.status : "",
    // loginError: state.login.loginError
  };
};

export default connect(mapStateToProps)(Login);
