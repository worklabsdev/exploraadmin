import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import explora from "../images/explora.png";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  //NavItem,
  //NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  //Table
} from "reactstrap";
import { reset_state } from "../actions";
class Header extends Component {
  constructor(props) {
    super(props);
  }
  logout = () => {
    localStorage.clear();
    var { dispatch } = this.props;
    // dispatch(login_admin(userjson));
    dispatch(reset_state());
    this.props.history.push("/");
  };
  render() {
    return (
      <div>
        <Navbar color="dark" dark expand="md">
          <NavbarBrand href="/">
            {" "}
            <img src={explora}
                style={{ width: 150, height: 45}}
                className="imagelogin"
              />
          </NavbarBrand>{" "}
          <NavbarToggler onClick={this.props.toggle} />{" "}
          <Collapse isOpen={this.props.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  User{" "}
                </DropdownToggle>{" "}
                <DropdownMenu right>
                  <DropdownItem>Change Password </DropdownItem>
                  <DropdownItem>Setting's </DropdownItem>{" "}
                  <DropdownItem onClick={this.logout}>Logout </DropdownItem>{" "}
                  <DropdownItem divider />
                </DropdownMenu>{" "}
              </UncontrolledDropdown>{" "}
            </Nav>{" "}
          </Collapse>{" "}
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.login.isLoggedin
  };
};
// const mapstate

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps
// )(Header);

export default withRouter(connect(mapStateToProps)(Header));
