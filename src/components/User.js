import React, { Component } from "react";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import { connect } from "react-redux";
import { getUsers } from "../actions/index";
import {
  Container,
  Row,
  Col,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,Label,
  Input
} from "reactstrap";

// const sidebarstyle = {
//   height: "690px",
//   background: "#eae9e9",
//   boxShadow: "3px 1px 3px silver",
//   paddingTop: "0px"
// };
// const imgSidebar = {
//   height: "140px",
//   width: "108%",
//   marginLeft: "15px;"
// };
const down = {
  marginTop: "20px"
};

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openNavDropDown: false,
      modalIModalDel: false,
      modalIModalEdit: false,
      interestname: ""
    };
  }
  componentDidMount() {
    this.props.dispatch(getUsers());
  }

  toggleDropdown = () => {
    this.setState({
      openNavDropDown: !this.state.openNavDropDown
    });
  };
  toggleAddInterestModalDel = () => {
    this.setState(prevState => ({
      modalIModalDel: !prevState.modalIModalDel
    }));
  };
  toggleAddInterestModalEdit = () => {
    this.setState(prevState => ({
      modalIModalEdit: !prevState.modalIModalEdit
    }));
  };
  handleChange = e => {
    console.log(e);
    this.setState({ [e.target.name]: e.target.value });
  };
  render() {
    return (
      <div>
        <Header toggle={this.toggleDropdown} />
        <Row>
          <Col xs="4" sm="2" className="sidebarstyle">
            <Sidebar />
          </Col>
          <Col>
            <Container>
              <h2 style={down} align="left">
                User's
              </h2>
              <Table>
                <thead>
                  <tr>
                    <th>S.No.</th>
                    <th>First Name</th>
                    <th>Email</th>
                    <th>Created At</th>
                    <th>Actions</th>
                  </tr>
                </thead>

                {this.props.getUserDataLoading == true ? (
                  <div> Loading... </div>
                ) : (
                  <tbody>
                    {this.props.usersData.length > 0 &&
                      this.props.usersData.map((user, index) => {
                        console.log("these are users", user);
                        return (
                          <tr key={user._id}>
                            <td>{index + 1}</td>
                            <td>{user.name}</td>
                            <td>{user.email}</td>
                            <td>{new Date(user.createdAt).toLocaleDateString()} {new Date(user.createdAt).toLocaleTimeString()}</td>
                            <td>
                              <i className="fa fa-edit" onClick={this.toggleAddInterestModalEdit}/> &nbsp;{" "}
                              <i className="fa fa-trash" onClick={this.toggleAddInterestModalDel}/>
                            </td>
                          </tr>
                        );
                      })}
                  </tbody>
                )}
              </Table>
            </Container>
          </Col>
        </Row>
        <Modal
          isOpen={this.state.modalIModalEdit}
          toggle={this.toggleAddInterestModalEdit}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleAddInterestModalEdit}>
            Edit Information
          </ModalHeader>
          <ModalBody>
            <Label for="Interest">First Name</Label>
            <Input
              type="text"
              name="interestname"
              value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              // placeholder="Enter Interest"
            /> 
            <Label for="Interest" style={{marginTop:"3.5%"}}>E-mail Address</Label>
            <Input
              type="text"
              name="interestname"
              value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              // placeholder="Enter Interest"
            />
          <Button className="pull-right" style={{marginTop:"4%"}}>
              Ok
              {/* {this.props.interestLoading &&
              this.props.interestLoading == true ? (
                <i className="fa fa-spinner fa-spin" />
              ) : (
                ""
              )} */}
            </Button>
            {/* </Form> */}
          </ModalBody>
        </Modal>


        <Modal
          isOpen={this.state.modalIModalDel}
          toggle={this.toggleAddInterestModalDel}
          className={this.props.className}
        >
          <ModalHeader toggle={this.toggleAddInterestModalDel}>
           Delete
          </ModalHeader>
          <ModalBody>
            <Label for="Interest">Are You sure to Delete !!</Label>
            <Button className="btn btn-danger" style={{marginLeft:"85%"}} >
              Delete
              {/* {this.props.interestLoading &&
              this.props.interestLoading == true ? (
                <i className="fa fa-spinner fa-spin" />
              ) : (
                ""
              )} */}
            </Button>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  getUserDataLoading: state.users.getUserDataLoading,
  usersData: state.users.usersData
});

export default connect(mapStateToProps)(User);
