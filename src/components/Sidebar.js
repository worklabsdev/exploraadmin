import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import explora from "../images/explora.png";
export default class Sidebar extends Component {
  render() {
    return (
      <div>
        <div style={{background:"#ff0000", marginRight:"-7.8%", padding:"20%"}}>
        <img src={explora} 
        className="imgSidebar" 
        style={{ width: 65, height: 20}}
        />
        </div>
        
        <ul className="sidebarlist">
          <li>
            <NavLink to="/users" activeClassName="activeli" exact={true}>
              Manage Users
            </NavLink>
          </li>
          <li>
            <NavLink to="/interests" activeClassName="activeli" exact={true}>
              Manage Interest
            </NavLink>
          </li>

          <li>
            <NavLink to="/test" activeClassName="activeli" exact={true}>
              Manage Activities
            </NavLink>
          </li>
        </ul>
      </div>
    );
  }
}
