import React, { Component } from "react";
import Header from "../components/Header";
import Sidebar from "../components/Sidebar";
import { connect } from "react-redux";
import { getUsers } from "../actions/index";
import { getActivity } from "../actions/index";
import {getDeleteActivity} from "../actions/index";
import {
  Container,
  Row,
  Col,
  Table,
  Button,
  Modal,
  ModalHeader,
  ModalBody,Label,
  Input
} from "reactstrap";

const sidebarstyle = {
  height: "690px",
  background: "#eae9e9",
  boxShadow: "3px 1px 3px silver",
  paddingTop: "0px"
};
const imgSidebar = {
  height: "140px",
  width: "108%",
  marginLeft: "15px;"
};
const down = {
  marginTop: "20px"
};
class Manageactitvities extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openNavDropDown: false,
      modalIModalDel: false,
      modalIModalEdit: false,
      interestname: "",
      idToDelete: ''
    };
  }
 
  componentDidMount() {
    this.props.dispatch(getUsers());
    this.props.dispatch(getActivity());
    
  }

  toggleDropdown = () => {
    this.setState({
      openNavDropDown: !this.state.openNavDropDown
    });
  };
  toggleAddInterestModalDel = (id) => {
    this.setState(prevState => ({
      modalIModalDel: !prevState.modalIModalDel,
      idToDelete: id
    }));
  };
  toggleAddInterestModalEdit = () => {
    this.setState(prevState => ({
      modalIModalEdit: !prevState.modalIModalEdit
    }));
  };
  handleChange = e => {
    console.log(e);
    this.setState({ [e.target.name]: e.target.value });
  };
  deleteActivity = () => {
    const {idToDelete} = this.state;
    this.props.dispatch(getDeleteActivity(idToDelete))
  }


  render() {
    return (
      <div>
      <Header toggle={this.toggleDropdown} />
      <Row>
        <Col xs="4" sm="2" className="sidebarstyle">
          <Sidebar />
        </Col>
        <Col>
          <Container>
            <h2 style={down} align="left">
              Activities
            </h2>
            <Table>
              <thead>
                
                <tr>
                <th>S.No.</th>
                  <th>Activity</th>
                  <th>Date</th>
                  <th>Location</th>
                  <th>Status</th>
                  <th>User</th>
                  <th>Actions</th>
                </tr>
              </thead>

              {this.props.getUsersActivity === true ? (
                <div> Loading... </div>
              ) : (
                <tbody>
                  {this.props.usersData.length > 0 &&
                    this.props.usersActivity.map((user, index) => {
                      console.log("these are users", user);
                      return (
                        <tr key={user._id}>
                          <td>{index+1}</td>
                          <td>{user.activity} &nbsp;{""}<i className="fa fa-edit" onClick={this.toggleAddInterestModalEdit}/> </td>
                          <td>{new Date(user.createdAt).toLocaleDateString()} {new Date(user.createdAt).toLocaleTimeString()}</td>
                          <td>{user.location}</td>
                          <td>{user.status}</td>
                          <td>{user.name}</td>
                          <td>
                            <i className="fa fa-trash" onClick={() => this.toggleAddInterestModalDel(user._id)}/>
                          </td>
                        </tr>
                      );
                    })}
                </tbody>
              )}
            </Table>
          </Container>
        </Col>
      </Row>
      <Modal
        isOpen={this.state.modalIModalEdit}
        toggle={this.toggleAddInterestModalEdit}
        className={this.props.className}
      >
        <ModalHeader toggle={this.toggleAddInterestModalEdit}>
          Edit Information
        </ModalHeader>
        <ModalBody>
        <Label for="Interest">Activity</Label>
            <Input
              type="text"
              name="interestname"
              // value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              // placeholder="Enter Interest"
            /> 
            <Label for="Interest">Location</Label>
            <Input
              type="text"
              name="interestname"
              // value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              // placeholder="Enter Interest"
            /> 
            <Label for="Interest">User</Label>
            <Input
              type="text"
              name="interestname"
              // value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              // placeholder="Enter Interest"
            /> 
            <Label for="Interest">Description</Label>
            <Input
              type="text"
              name="interestname"
              // value={this.state.interestname}
              onChange={e => this.handleChange(e)}
              // placeholder="Enter Interest"
            /> 
            <Label for="Interest">About</Label>
            <Input
              type="text"
              name="interestname"
              // value={this.state.interestname}
              onChange={e => this.handleChange  (e)}
              // placeholder="Enter Interest"
            /> 
          <Button className="pull-right" style={{marginTop:"4%"}}>
            Ok
            {/* {this.props.interestLoading &&
            this.props.interestLoading == true ? (
              <i className="fa fa-spinner fa-spin" />
            ) : (
              ""
            )} */}
          </Button>
          {/* </Form> */}
        </ModalBody>
      </Modal>


      <Modal
        isOpen={this.state.modalIModalDel}
        toggle={this.toggleAddInterestModalDel}
        className={this.props.className}
      >
 <ModalHeader toggle={this.toggleAddInterestModalDel}>
         Delete
        </ModalHeader>
        <ModalBody>
          <Label for="Interest">Are You sure to Delete !!</Label>
          <Button className="btn btn-danger" style={{marginLeft:"85%"}} onClick ={this.deleteActivity} >
            Delete
            {/* {this.props.interestLoading &&
            this.props.interestLoading == true ? (
              <i className="fa fa-spinner fa-spin" />
            ) : (
              ""
            )} */}
          </Button>
        </ModalBody>
      </Modal>
    </div>
    );
  }
}

const mapStateToProps = state => ({
  
  getUsersActivity: state.getUsersActivity,
  usersData: state.users.usersData,
  usersActivity: state.ma.usersActivity,
  getDeleteActivity: state.getDeleteActivity,
  usersDeleteActivity:state.deleteactivityreducer.usersDeleteActivity
});

export default connect(mapStateToProps)(Manageactitvities);

